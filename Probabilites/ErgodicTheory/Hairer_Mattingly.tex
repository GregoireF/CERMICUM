


In applied a theoretical probability, it is often interesting to study the long time
behaviour of a homogeneous Markov chain~$(x_n)_{n\in\mathbb{N}}$ taking value in a
space~$\mathcal{X}$. This is done in general by considering the law of the Markov chain
\index{Markov chain} at time~$n\in\mathbb{N}$ against test functions~$\varphi$, that is
\[
\mathbb{E}_x[\varphi(x_n)],
\]
where~$\mathbb{E}_x$ denotes the expectation with respect to all the realizations of the Markov
chain starting at a point $x\in \mathcal{X}$. Before considering the long time behaviour of
such a chain, one can study the existence of an invariant
measure~$\mu^*\in\mathcal{P}(\mathcal{X})$, \index{invariant measure} that is a measure
such that, if~$x_0$ is distributed according to~$\mu^*$,
then $x_n$ is also distributed according to~$\mu^*$ for all~$n\geq 1$. It is natural to ask,
for a given Markov chain, whether there exists a (unique) invariant measure~$\mu^*$ and whether
the Markov chain is ergodic with respect to this invariant measure, in the sense that the
law of~$x_n$ should converge to~$\mu^*$ for some class of test functions~$\varphi$.


There is a vast literature on the existence of an invariant measure and ergodicity of Markov
chains, starting with the works of A. Markov in the 1910's. We propose here a recent version
provided by Martin Hairer and Jonathan Mattingly~\cite{hairer2011yet}, which has two
advantages: (i) the assumptions are reasonable to check in practice,
(ii) the proof is easy and elegant.


In what follows, we consider a measurable space~$\mathcal{X}$ and denote by~$P$ the
evolution operator of the Markov chain, that is, for any bounded measurable
function~$\varphi$,
\[
P\varphi (x) = \mathbb{E}_x[\varphi(x_1)].
\]
The theorem relies on the two following assumptions.
%
\begin{assumption}
  \label{as:hairer2011yetL}
  There exist a measurable function $W:\mathcal{X}\to[0, +\infty]$ and constants
  $C \geq 0$, $\gamma \in (0,1)$  such that
    \begin{equation}
      \label{eq:lyapunov}
      \forall \, x\in \mathcal{X}, \quad (PW)(x) \leq \gamma W(x) + C.
    \end{equation}  
\end{assumption}
\index{Lyapunov function} Loosely speaking, the latter assumption means that there exists some
energy function~$W$ that decreases in average along trajectories of the Markov chain. Such a
function is called a Lyapunov function, and is associated to the following functional space
\[
B_W(\mathcal{X}) = \left\{ \varphi\ \big| \ \sup_{\mathcal{X}}
\frac{|\varphi|}{1 + W} < +\infty \right\}.
\]
The second key ingredient in the ergodicity of $P$ is the minorization condition, which can
be stated as follows.

\begin{assumption}
    \label{as:hairer2011yetM}
  There exist $\alpha\in (0,1)$ and $\eta\in\mathcal{P}(\mathcal{X})$ such that
  \[
  \underset{x\in\mathcal{C}}{\inf}\, P(x, \cdot) \geq \alpha \eta( \cdot ),
  \]
  where $\mathcal{C}=\{ x\in\mathcal{X} \, | \, W(x) \leq R  \}$ for some $R> 2 C/(1-\gamma)$,
  and $\gamma$, $C$ are the constants from Assumption~\ref{as:hairer2011yetL}.
\end{assumption}
The following result holds under these conditions (see~\cite[Theorem 1.2]{hairer2011yet}).
\index{ergodicity}
\begin{theorem}
Let Assumptions~\ref{as:hairer2011yetL} and~\ref{as:hairer2011yetM} hold.
Then, $P$ has a unique invariant measure $\mu^*$, which is such that $\mu^*(W)<+\infty$. Moreover,
there exist $C >0$ and $\bar{\alpha}\in (0,1)$ such that, for any $\varphi\in B_W(\mathcal{X})$,
\[
\forall\, n\geq 0, \quad \| P^n \varphi - \mu^*(\varphi) \|_{B_W}
\leq C \bar{\alpha}^n \| \varphi - \mu^*(\varphi) \|_{B_W}.
\]
\end{theorem}
This theorem is very powerful since it provides three information:
\begin{itemize}
\item the existence of a unique invariant measure;
\item the ergodicity of the dynamics with respect to this invariant measure;
\item a class of (a priori) unbounded functions for which convergence holds, and the corresponding
  integrability of the invariant measure.
\end{itemize}
More details are provided in~\cite{hairer2011yet} along with a self-contained proof, which
relies on a Banach fixed point theorem.
